class Category < ApplicationRecord
  has_many :products, :dependent => :delete_all
  def self.add_slugs
    update(slug: to_slug(name))
  end

  def to_param
    slug
  end
  
end
