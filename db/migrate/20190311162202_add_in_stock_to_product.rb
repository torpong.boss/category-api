class AddInStockToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :inStock, :integer
  end
end
