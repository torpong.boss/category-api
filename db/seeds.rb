# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Category.create({ name: 'furnitures', slug: 'furniture'})
Category.create({ name: 'games', slug: 'games'})
Category.create({ name: 'books', slug: 'books'})
Category.create({ name: 'food', slug: 'snack'})
Category.create({ name: 'furnitures', slug: 'furniture-supply'})
Category.create({ name: 'glectronics&gadgets', slug: 'gadgets'})

Product.create({ name: 'chair', inStock: '12', category_id: '1'})
Product.create({ name: 'table', inStock: '5', category_id: '1'})
Product.create({ name: 'chair-supply', inStock: '73', category_id: '5'})
Product.create({ name: 'arm-chair', inStock: '51', category_id: '5'})
Product.create({ name: 'ps4-disc', inStock: '40', category_id: '2'})
Product.create({ name: 'computer-science-book', inStock: '70', category_id: '3'})
Product.create({ name: 'lasagne', inStock: '3', category_id: '3'})
Product.create({ name: 'pizza', inStock: '9', category_id: '3'})
Product.create({ name: 'walky-talky', inStock: '10', category_id: '6'})
Product.create({ name: 'phone', inStock: '2', category_id: '6'})
Product.create({ name: 'headlamps', inStock: '12', category_id: '6'})
Product.create({ name: 'gameboy-advance', inStock: '99', category_id: '2'})
Product.create({ name: 'King-Arthur', inStock: '66', category_id: '3'})
Product.create({ name: 'percy jackson', inStock: '84', category_id: '3'})