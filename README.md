# README

simple category api 
get /Products
get /Products/:id
get /Categories
get /Category/:id

POST Category: { name: string }

Product: { name: string, inStock: integer, category_id: integer (referred to category id) }